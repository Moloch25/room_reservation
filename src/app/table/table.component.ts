import { Component, NgModule, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Store } from '@ngrx/store';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ListRoom, PropetlyRoom} from '../store/models/rooms.model';
import {PersonBookRoom} from '../store/models/user.model';

import { roomReducer } from '../store/reducers/rooms.reducer';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit {


  person:PersonBookRoom={
    userName: "",
    dateStart: "",
    dateEnd: "",
    numberRooms: 0
  };
  message: String = "";
  closeResult = '';
  title ="Бронирование номера";
  dt = new Date();
  countRows = 5;
  dateStart= this.datepipe.transform(this.dt, 'yyyy-MM-dd');
  dateEnd= this.datepipe.transform(this.dt, 'yyyy-MM-dd');
  bookRoom:ListRoom[] = [];

  freeRoomDate: PropetlyRoom[] = [
    {numberRoom: 345, countPlace: 3, price:300},
    {numberRoom: 346, countPlace: 2, price:200},
    {numberRoom: 347, countPlace: 4, price:400},
    {numberRoom: 348, countPlace: 5, price:500},
    {numberRoom: 349, countPlace: 6, price:600},
    {numberRoom: 350, countPlace: 1, price:100},
  ]

  constructor(
    public datepipe: DatePipe,
    private modalService: NgbModal,
    private store: Store
  ) { 

    }

  ngOnInit(): void {
    this.getCountFreeRooms();
  }


  onShowForm(t: Number, content: any){    
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    if( typeof(t) !== "undefined" && t!=null){
      this.person.numberRooms = t;
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onCLoseForm(n: number) {
    if(n == 1) {
      this.person.numberRooms = 0;
      this.person.dateEnd = "";
      this.person.dateStart = "";
      this.person.userName = "";
      this.modalService.dismissAll();
    } else {
      this.message = "Номер "+this.person.numberRooms+" забронирован! Дата въезда: "+this.person.dateStart+" Дата выезда: "+this.person.dateEnd;
      this.modalService.dismissAll();
    }
    
  }

  getCountFreeRooms(){
    for(let i=0; i<this.bookRoom.length; i++){
      if(this.bookRoom[i].date == this.dateStart){
        this.freeRoomDate = [];
        this.freeRoomDate = JSON.parse(JSON.stringify(this.bookRoom[i].value));
        break;
      }
    }
    if(this.dateStart! <= this.dateEnd!){
      let dt = this.dateStart!;
        while(dt != this.dateEnd){
          let fdt = Date.parse(dt);
          dt = this.datepipe.transform(new Date(fdt + 24 * 60 * 60 * 1000), 'yyyy-MM-dd')!;
          let vbn = [];
          for(let i=0; i<this.bookRoom.length; i++){
            if(this.bookRoom[i].date == dt){
              if(this.freeRoomDate != this.bookRoom[i].value){
                for(let f = 0; f < this.freeRoomDate.length; f++){
                  let g = false;
                  for(let r = 0; r < this.bookRoom[i].value.length; r++)
                  {
                    if(this.freeRoomDate[f].numberRoom==this.bookRoom[i].value[r].numberRoom){
                      g = true;
                      break;
                    }
                  }
                  if(g==false){
                    vbn.push(f);
                  }
                }
              }
            }
          }
          for(let i=vbn.length-1; i>=0; i--){
              this.freeRoomDate.splice(vbn[i],1);
          }

          
        }
    }
    if(this.countRows<this.freeRoomDate.length-1){      
      for(let f = this.freeRoomDate.length-1; f >= this.countRows; f--){
        this.freeRoomDate.splice(f,1);
      }
    }
    
  }

  onListPerson(event: any, typeData: number){
    if(event.target.id=="NameUser") {
      this.person.userName = event.target.value;
    }
    if(event.target.id=="dateOfStart") {
      this.person.dateStart = event.target.value;
    }
    if(event.target.id=="dateOfEnd") {
      this.person.dateEnd = event.target.value;
    }
  }

  onDateStart(event: any, typeDAte: number) {
    if(typeDAte == 1) {
      this.dateStart = event.target.value;
    } else {
      this.dateEnd = event.target.value;
    }    
    this.getCountFreeRooms();
  }

  onCountRow(event: any){
    console.log(this.countRows);
    this.countRows = event.target.value;
    this.getCountFreeRooms();
  }

  onPriceSort(event: any){
    if(event.target.value == 1) {
      this.freeRoomDate.sort(function (a, b) {
        if (a.price > b.price) {
          return 1;
        }
        if (a.price < b.price) {
          return -1;
        }
        return 0;
      })
    }
    if(event.target.value == 2) {
      this.freeRoomDate.sort(function (a, b) {
        if (a.price > b.price) {
          return -1;
        }
        if (a.price < b.price) {
          return 1;
        }
        return 0;
      })
    }
    if(event.target.value == 0) {
      this.getCountFreeRooms();
    }
  }

}
