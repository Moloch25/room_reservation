import { Action } from '@ngrx/store';
import {PropetlyRoom, ListRoom} from '../models/rooms.model';
import {RoomUnion, RoomsAction} from '../action/rooms.action';

export interface State {
    freeRooms: ListRoom[];
  }

export const initialState: State = {

    freeRooms : [
        {
            date: '2021-07-04',
            value: [
              {numberRoom: 345, countPlace: 3, price:300},
              {numberRoom: 346, countPlace: 2, price:200},
              {numberRoom: 347, countPlace: 4, price:400},
              {numberRoom: 348, countPlace: 5, price:500},
              {numberRoom: 349, countPlace: 6, price:600},
              {numberRoom: 350, countPlace: 1, price:100},
              {numberRoom: 351, countPlace: 2, price:200},
              {numberRoom: 352, countPlace: 2, price:200},
              {numberRoom: 353, countPlace: 2, price:200},
              {numberRoom: 354, countPlace: 2, price:200},
              {numberRoom: 355, countPlace: 2, price:200},
              {numberRoom: 356, countPlace: 2, price:200},
              {numberRoom: 357, countPlace: 2, price:200},
              {numberRoom: 358, countPlace: 2, price:200},
              {numberRoom: 359, countPlace: 2, price:200},
              {numberRoom: 360, countPlace: 2, price:200},
              {numberRoom: 361, countPlace: 2, price:200},
              {numberRoom: 362, countPlace: 2, price:200},
              {numberRoom: 363, countPlace: 2, price:200},
              {numberRoom: 364, countPlace: 2, price:200},
              {numberRoom: 365, countPlace: 2, price:200},
              {numberRoom: 366, countPlace: 2, price:200},
              {numberRoom: 367, countPlace: 2, price:200},
              {numberRoom: 368, countPlace: 2, price:200},
            ]
          },
          {
            date: '2021-08-05',
            value: [
              {numberRoom: 345, countPlace: 3, price:300},
              {numberRoom: 346, countPlace: 2, price:200},
              {numberRoom: 347, countPlace: 4, price:400},
              {numberRoom: 348, countPlace: 5, price:500},
              {numberRoom: 349, countPlace: 6, price:600},
              {numberRoom: 352, countPlace: 1, price:100},
            ]
          },
          {
            date: '2021-09-06',
            value: [
              {numberRoom: 345, countPlace: 3, price:300},
              {numberRoom: 346, countPlace: 2, price:200},
              {numberRoom: 347, countPlace: 4, price:400},
              {numberRoom: 348, countPlace: 5, price:500},
              {numberRoom: 349, countPlace: 6, price:600},
              {numberRoom: 353, countPlace: 1, price:100},
            ]
          },
          {
            date: '2021-10-07',
            value: [
              {numberRoom: 345, countPlace: 3, price:300},
              {numberRoom: 346, countPlace: 2, price:200},
              {numberRoom: 347, countPlace: 4, price:400},
              {numberRoom: 348, countPlace: 5, price:500},
              {numberRoom: 349, countPlace: 6, price:600},
              {numberRoom: 354, countPlace: 1, price:100},
            ]
          },
          {
            date: '2021-11-08',
            value: [
              {numberRoom: 345, countPlace: 3, price:300},
              {numberRoom: 346, countPlace: 2, price:200},
              {numberRoom: 347, countPlace: 4, price:400},
              {numberRoom: 348, countPlace: 5, price:500},
              {numberRoom: 349, countPlace: 6, price:600},
              {numberRoom: 355, countPlace: 1, price:100},
            ]
          },
          {
            date: '2021-12-09',
            value: [
              {numberRoom: 344, countPlace: 3, price:300},
              {numberRoom: 346, countPlace: 2, price:200},
              {numberRoom: 347, countPlace: 4, price:400},
              {numberRoom: 348, countPlace: 5, price:500},
              {numberRoom: 349, countPlace: 6, price:600},
              {numberRoom: 356, countPlace: 1, price:100},
            ]
          },
      ],
}

export function roomReducer(
    state: State = initialState,
    action: RoomUnion
) {
    switch (action.type) {
        case RoomsAction.GetAllRoomsFree:
          return {
            ...state,
          }
        case RoomsAction.SortDateRoom:
          return {
            ...state,
          }
        default:
          return state
      }
}

export const getRoomsFree = (state: State) => state.freeRooms;