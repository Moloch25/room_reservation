import { Action } from '@ngrx/store';
import {PersonBookRoom} from '../models/user.model';
import * as User from '../action/user.actiom';

export interface State {
    Person: PersonBookRoom;
  }

export const initialState: State = {
    Person: {
        userName:"",
        dateEnd:"",
        dateStart:"",
        numberRooms: 0
    }
}

export function userReducer(
    state: State = initialState,
    action: User.UserUnion
) {
    switch (action.type) {
        case User.UserAction.GetUser:
          return {
            ...state,
          }
        case  User.UserAction.SetUser:
          return {
            ...state,
          }
        default:
          return state
      }
}

export const selectUser = (state: State) => state.Person;
