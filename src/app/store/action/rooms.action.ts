import {Action} from '@ngrx/store';

export enum RoomsAction {
    GetAllRoomsFree = '[tiket page] GetAllRoomsFree',
    SortDateRoom = '[tiket page] SortDataFree',
}

export class GetAllRoomsFree implements Action {
    readonly type = RoomsAction.GetAllRoomsFree
}

export class SortDateRoom implements Action {
    readonly type = RoomsAction.SortDateRoom
    // constructor(public payload: {dateS: string, dateE: string}){

    // }
}

export type RoomUnion = GetAllRoomsFree | SortDateRoom;