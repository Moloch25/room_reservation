import {Action} from '@ngrx/store';

export enum UserAction {
    GetUser = '[tiket page] GetUser',
    SetUser = '[tiket page] SetUser',
}

export class GetUser implements Action {
    readonly type = UserAction.GetUser
}

export class SetUser implements Action {
    readonly type = UserAction.SetUser
    // constructor(public payload: {dateS: string, dateE: string}){

    // }
}

export type UserUnion = GetUser | SetUser;