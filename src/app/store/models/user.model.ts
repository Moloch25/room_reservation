export interface PersonBookRoom{
    userName: String,
    dateStart: String,
    dateEnd: String,
    numberRooms: Number,
  }