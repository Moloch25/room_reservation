import { ActionReducerMap } from '@ngrx/store';
import * as User from './reducers/user.reducer'
import * as Room from './reducers/rooms.reducer'

export interface State {
    users: User.State,
    rooms: Room.State,
}

export const reducers: ActionReducerMap<State> = {
    users: User.userReducer,
    rooms: Room.roomReducer,
}